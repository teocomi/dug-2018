---
typora-copy-images-to: assets
---

# The future of data exchange is here: learn how to stream data in real time with Speckle Works



## Summary

Speckle is an open source platform for exchanging data across AEC software, it is the AEC & design data communication platform of the future! In this class you’ll learn more about Speckle, we’ll see how to set up the Dynamo client and how to share data in real time to other Dynamo or Grasshopper clients. We’ll also see how to send custom objects and how to extend it by contributing to Speckle Open Source community.

### Learning objectives

- Learn what Speckle is and how it came to life
- Learn how to use Speckle to stream data in real time in your projects
- Learn how to contribute to Speckle and be part of its Open Source community

## Requirements

- Dynamo 2.0.1
- Revit 2018 or newer



## Speckle!

### What is Speckle?

![0259F9BC-D5D9-4554-8A4F-F487058AE6CB](./assets/0259F9BC-D5D9-4554-8A4F-F487058AE6CB.png)

Speckle is an open source data communication protocol and platform for the AEC industry. 

With Speckle data generated in various design tools can be streamed and exchanged via custom integrations (clients) through the Speckle Server, these so far include plugins for:

- Grasshopper
- Rhino
- Dynamo
- Revit (under development)
- Blender
- Unity (experimental)

Let's see a quick example of points and lines exchanged between Dynamo and Grasshopper:

![speckle-demo](https://user-images.githubusercontent.com/2679513/41601159-99a0499a-73cf-11e8-90b9-36b43a822076.gif)

Or the generation of Revit elements from Grasshopper:

https://twitter.com/speckle_works/status/1059799437340491776

Or again the use of Speckle to stream geometry to the Hololens:

https://www.youtube.com/watch?v=MFDeAb54dOc

#### Speckle Community

Speckle is powered by a growing community of 400+ contributors and users, you can join the discussion by entering the Slack channel:

https://slacker.speckle.works/

Then you can either click Speckle > Manage Accounts or place a new Sender/Receiver node and if you don't have any account you'll be prompted to create one:

![1541762921818](./assets/1541762921818.png)



#### Show me the code!

All the Speckle code is open source with an MIT license, meaning you can do whatever you want with it! All the code is hosted on [GitHub](https://github.com/speckleworks/).

![5D7C0272-56DA-4E15-936D-E42C2E840AFB](./assets/5D7C0272-56DA-4E15-936D-E42C2E840AFB.png)



## Installation

Speckle in now distributed under a single installer, currently for Grasshopper, Rhino and Dynamo.

- Download the installer from: https://github.com/speckleworks/SpeckleInstaller/releases/latest
  ![1543403081670](./assets/1543403081670.png)
- Close Rhino&Grasshopper
- Close Dynamo
- Run it

![1543402942562](./assets/1543402942562.png)



You now have Speckle for Rhino, **Speckle for Grasshopper** and Speckle for Dynamo installed!

![1537456901566](./assets/1537456901566.png)



![1537457187824](./assets/1537457187824.png)



## Account creation

To use Speckle you need a Speckle Account, the first time you use a Speckle plugin you'll be prompted for your details, but you'll also be able to create one.

You only need one Speckle Account to use Speckle, but you could have more if, for instance, your company was running a self hosted version. 

Let's go ahead and create one, you can do so from the Grasshopper **or** the Dynamo client.

In **Grasshopper**, drag and drop the *Data Sender* or *Data Receiver* components on the canvas.

![1537457464215](./assets/1537457464215.png)

In **Dynamo**, click on the *Speckle Sender* or *Speckle Receiver* to place the on the graph.

![1537457525739](./assets/1537457525739.png)

(Yes, I agree with you, we need to standardize naming, but at least icons are the same!)

Now, in both cases, you'll see the following window:

![1537457889305](./assets/1537457889305.png)

For *Server Url* use:

`https://hestia.speckle.works/api/v1`

Unless, of course, you [deployed your own](https://speckle.works/doc/deployaserver/). Fill in the other fields and click *Register*!

If everything went smooth so far, we'll be ready to start sending and receiving data. If not, **get in touch**, and we'll try to troubleshoot:

- on the [forum](https://discourse.speckle.works/), for general discussions
- on [github](https://github.com/speckleworks), to report bugs/feature requests
- on [slack](http://speckle-works.slack.com), to chat with the community



### Changing account

In **Grasshopper**, each time you place a component you'll be prompted to select what account to use.

In **Dynamo**, after selecting an account, it becomes the **default** one. But you can change it and add new ones at any time from *View > Speckle Accounts*:

![1537461939189](./assets/1537461939189.png)





## Hello Speckle!

Let's send and receive our first **stream**! A stream, in Speckle terms, is a set of data that can be sent and received. A stream has a unique ID (**streamId**) used to identify it, it also has **layers**, which are used to group data in the stream.

More info on streams can be found in the [Speckle docs](https://speckle.works/doc/coredatamodels/).

In **Grasshopper**, place a Sender component (or reuse the one created previously) and create a panel with some text, then connect the panel to one of the input ports of the Sender, just like so:

![1537459297992](./assets/1537459297992.png)

Now, right click on the Sender and then select to copy the streamId to clipboard:

![1537459159288](./assets/1537459159288.png)

In **Dynamo**, place a Receiver node:

![1537459420963](./assets/1537459420963.png)

Then, either paste the streamId or use the friendly *paste button*, and....

![mind blown](https://media1.giphy.com/media/xT0xeJpnrWC4XWblEk/giphy.gif)



Here's a full gif of the process, it took us 4 mouse clicks, but we made it! We now  have real time data streaming between Grasshopper and Dynamo. Just try changing the text, and you'll see it update automatically.

![speckle-basic](./assets/speckle-basic.gif)



## Working with streams 

Let's now go through other features and things that can be done with streams.

### Set a stream name

Optional, but good to keep our streams organized. 

In **Grasshopper**, right click on the Sender and set the component Name:

![1537461209453](./assets/1537461209453.png)

In **Dynamo**, similarly, right click and select *Rename Node...*:

![1537461278786](./assets/1537461278786.png)

### **Find a stream by name**

In **Grasshopper**, you'll need to use a few components, more specifically the Accounts + Streams + Expand Object:

![1537461555475](./assets/1537461555475.png)

In **Dynamo**, just use the Streams node and select/search the name:

![speckle-basic2](./assets/speckle-basic2.gif)

### Add/Remove inputs

You can add and remove inputs from a Sender component/node, these correspond to the **stream layers**, in Speckle terms.

In **Grasshopper**, zoom on the component until you see these + and - signs:

![1537462182235](./assets/1537462182235.png)

In **Dynamo**, click on the + and - buttons:

![1537462250778](./assets/1537462250778.png)

### Rename inputs/layers

In **Grasshopper**, right click on the **input** itself:

![1537462494319](./assets/1537462494319.png)

In **Dynamo**, right click on the *node > Rename Inputs...*:

![1537462570526](./assets/1537462570526.png)

### Support for multiple data types

Speckle supports natively many data types like text, numbers, geometry primitives, lines, meshes etc... Here you can find a comprehensive list:

https://github.com/speckleworks/SpeckleDynamo/issues/10

![1537463244196](./assets/1537463244196.png)



### Support for nested lists / data trees

Speckle will automatically convert data trees to nested lists and vice versa:

![1537464067342](./assets/1537464067342.png)

### Add Custom User Data

Geometry sent with Speckle can have *User Data* attached to it, which is a way of adding *extra* information/properties/metadata. User Data is structured as a dictionary and you'll need ad-hoc *set* and *get* nodes/components to attach/retrieve it. 

![1537464815824](./assets/1537464815824.png)

## Advanced Features

### Speckle Viewer

The speckle viewer lets you see the content of your streams online.

To quickly check your stream online, right click > *View stream online*

![1543407544102](assets/1543407544102.png)



It's also possible to save versions of your streams as other streams!

Let's move deer to the side and save that as a new version:

![1543407623081](assets/1543407623081.png)

And then do it again.

From the viewer we'll be able to retrieve the older versions of the stream:

![history](assets/history.gif)

Check it put yourself: https://hestia.speckle.works/view/?streams=ewlMjM4N6



### Speckle Objects in Excel

Speckle streams can also be accessed via the Speckle API. That is extremely powerful, and even if you might not be familiar with Rest APIs, we can use it for instance to access a stream's objects from Excel.

Lets start by creating a bunch of points:

![1543416720169](.\assets\1543416720169.png)

And get its API url to the objects:

![1543417503046](.\assets\1543417503046.png)



That should open something like this in your browser:

`https://hestia.speckle.works/api/v1/streams/e4e_T60YR/objects?omit=displayValue,base64`

That looks like:

![1543417807328](.\assets\1543417807328.png)

Let's change the URL and its query parameter, so that it looks like:

`https://hestia.speckle.works/api/v1/streams/e4e_T60YR/objects?fields=value`

Now the endpoind is just returning the values associated with out objects and not a bunch of additional metadata:

![1543417938573](.\assets\1543417938573.png)



Now let's open *Excel > Data > From Web* and paste the URL:

![1543418014599](.\assets\1543418014599.png)

Click *connect*, then on *List*:

![1543418239680](D:\dug-2018\speckle\assets\1543418239680.png)

Click *To Table*, then *OK*:

![1543418831253](D:\dug-2018\speckle\assets\1543418831253.png)

Then click the little icon on top of the green column, then *OK*:

![1543418934000](D:\dug-2018\speckle\assets\1543418934000.png)

Then *Extract Values*, and select *comma*:

![1543419000033](D:\dug-2018\speckle\assets\1543419000033.png)

Now we can split the comma separated values:

![1543419124966](D:\dug-2018\speckle\assets\1543419124966.png)

And obtain a clean list of point coordinates:

![1543419187572](D:\dug-2018\speckle\assets\1543419187572.png)

If you close the query editor now, the generated table will keep sync with the stream and refresh each time you tell it to do so:

![1543419263217](D:\dug-2018\speckle\assets\1543419263217.png)