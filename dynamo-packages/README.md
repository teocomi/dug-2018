---
typora-copy-images-to: assets
---

# Everything you ever wanted to know about Dynamo packages: create, distribute and maintain them!



## Summary

In this class we'll see the different types of custom nodes and how to create them, we'll also see how to share them with your colleagues and the community. You will learn how to create a package from scratch and how to publish it using the Package Manager. You will also learn about best practices and how to maintain your packages once published.

### Learning objectives

- Learn about Custom Nodes
- Learn about different ways to create and publish your nodes as packages
- Learn about best practices for publishing and how to maintain your packages

## Requirements

- Dynamo 2.0.1
- Revit 2018 or newer



## What are custom nodes?

A custom node is a Dynamo node that's been created for a specific purpose and is not distributed with Dynamo by default. These nodes add new functionalities and push what you can do with Dynamo even further.

### Advantages

- Simplify the graph, keep it clean and intuitive
- Reusability
- Modularity, update all custom nodes of the same type at once
- Better use of conditional statements (if/then) and looping
- Add brand new functionalities
- Integrate with external services/libraries
- Understand better how Dynamo "thinks"
- Distribute with the package manager

### Disadvantages

- Less intuitive than visual programming
- Hard for novice users/learning curve
- Some custom nodes require compiling Dlls
- Need to be distributed separately to your dynamo definition
- With great power comes great responsibility, custom nodes are more prone to bugs, memory abuse or crashes

### Types of custom nodes

There are different types of custom nodes, for all levels and uses:

- *[Custom Node](http://dynamoprimer.com/en/09_Custom-Nodes/9-2_Creating.html)*, it’s done by nesting existing nodes into a container, the custom node is saved as a DYF file and can be easily shared. More advanced versions can contain:
  - *[Code Block](http://dynamobim.org/cbns-for-dummies/)*, a node that lets you add custom functionalities with DesignScript
  - *[Python Node](http://dynamoprimer.com/en/09_Custom-Nodes/9-4_Python.html)*, a node containing Python code, supports modules and packages ([template](https://github.com/DynamoDS/Dynamo/pull/8034)) 
- Advanced Custom Nodes:
  - *Zero Touch Node*, custom node written in .NET. It is a more complex choice but you will benefit of the .NET framework, a solid IDE, debugging tools and lots of libraries
  - *Explicit Custom Node*, basically a native Dynamo node written in .NET, it implements the NodeModel interface, can have a custom UI and affect the state of the graph



## Custom Nodes

### Custom Node with nodes

The easiest way to add new functionalities and to clean up your graph is to create a custom node from existing nodes, it basically groups a set of nodes into itself. Let's create a simple example. 

Open Dynamo and place these nodes:

![1542973202652](assets/1542973202652.png)

This definition takes two lists and swaps their first elements. Let's say you have to do this operation quite often, then it'd be a good idea to create a custom node out of it!

Select everything a part from the two code blocks, right click > Create Custom Node:

![1542973359920](assets/1542973359920.png)



You can now set the custom node name, description and category.  By using periods `.` you can nest the node into sub categories:

![1542973526749](assets/1542973526749.png)

![1542973548171](assets/1542973548171.png)

As we did that, Dynamo automatically opened a new tab for our custom node:

![1542973755461](assets/1542973755461.png)

This is the custom node editor, and you can recognize it by the yellow background. Notice that Dynamo automatically added some `Input` and `Output` nodes for us. If these were incorrect, you can edit them here, same applies to the rest of the custom node.

**IMPORTANT: save this file! The best place to put it, for now, is the default location suggested: `C:\Users\YOURUSERNAME\AppData\Roaming\Dynamo\Dynamo Core\2.0\definitions`**

Editing the custom node will be reflected in all its instances you have already placed.

Note: sharing this dynamo definition will now require sharing the custom nodes it uses as well.

### Custom Node with Code Block

Code Blocks are also nodes that let you add custom functionalities within Dynamo. Code blocks use DesignScript, a programming language and production modeling tool. DesignScript is the underlying technology Dynamo is based on.

CodeBlocks come in very useful when you want to quickly generate inputs, for instance:

![1542993312321](assets/1542993312321.png)

And with these these range expressions you can quickly generate numeric lists:

```c#
a = 1..5; // a = [1,2,3,4,5] start..end [using 1 as the default increment] Dynamo 'Range' node
b = 1..9..2; // b = [1,3,5,7,9] start..end..increment Dynamo 'Range' node
c = 1..9..~3; // c = [1.0, 3.666667, 6.333333, 9.0] start..end..~approximateIncrement
d = 1..9..#3; // d = [1, 5, 9] start..end..#noOfCases
e = 1..#3..9; // e = [1, 10, 19] start..#noOfCases..increment Dynamo 'Sequence' node
```

By placing a Code Block node inside a Custom Node, you'll be able to reuse the same code around your graph, and will only need to edit one instance of it when needed. As well you'll be able to distribute it in a package.

For instance Spring nodes is using DesignScript to generate a star geometry:

![1543156052326](assets/1543156052326.png)

![1542993910970](assets/1542993910970.png)

Let's try that code ourselves:

```c#
c1 = Circle.ByCenterPointRadius(p1,r1);
c2 = Circle.ByCenterPointRadius(p1,r1*(99.9-s)/100);
pts1 = c1.PointAtParameter(0..1..#n+1);
pts2a = c2.PointAtParameter(0..1..#n*2+1);
pts2b = DSCore.List.TakeEveryNthItem(pts2a,2,0);
pts3 = DSCore.List.Flatten(List.Transpose([pts1,pts2b]));
pts3c = DSCore.List.DropItems(pts3,-1);
star = PolyCurve.ByPoints(pts3c);
```

More on DesignScript can be found at the following links:

http://designscript.io/DesignScript_user_manual_0.1.pdf

https://d2f99xq7vri1nk.cloudfront.net/legacy_app_files/pdf/DesignScript_DMS_2011.pdf



### Custom Node with Python

Python is a very common programming language, and benefits from a vast variety of packages and modules. Python nodes work similarly to the Code Block, to access the editor we need to double click on the node:

![1543158993031](assets/1543158993031.png)

Inputs can be added by clicking the `+` sign on the node and then access in python from the `IN` variable, eg `IN[0]` will access the first input.

Python nodes can only have a single output eg `OUT = myoutputvariable`, but if that's provided as a list it can then be easily split into multiple ones with a Code Block:

![1543159788463](D:\dug-2018\dynamo-packages\assets\%5CUsers%5CMatteo%5CAppData%5CRoaming%5CTypora%5Ctypora-user-images%5C1543159788463.png)



That's a very common scenario especially when placing Python nodes inside a Custom Node.

Let's write a sample Python node to place cuboids of random height:

![1543161331631](assets\1543161331631.png)



```python
#import random
import sys
sys.path.append("C:\Program Files (x86)\IronPython 2.7\Lib")
import random
import clr
clr.AddReference('ProtoGeometry')
from Autodesk.DesignScript.Geometry import *

#The number of solids to array in the X and Y axes
count = IN[0]
#distance between eaxh solid
dist = IN[1]
#width and depth of each solid
size = IN[2]

#Create an empty list for the arrayed solids
solids = []

#Loop through X and Y
for i in range(count):
    for j in range(count):
        #create new cuboid and add it to the list
        point = Point.ByCoordinates(i*dist,j*dist,0)
        cuboid = Cuboid.ByLengths(point,size,size,random.randint(1,10))
        solids.append(cuboid)

OUT = solids
```

More on python nodes: http://primer.dynamobim.org/en/10_Custom-Nodes/10-4_Python.html

#### Replication in Python nodes

We have seen before how it's common practice to save Python nodes as Custom Nodes, to  make the most out of them. By default these nodes don't handle replication, but by setting their input type to `var` this can be easily achieved:

```python
import clr
clr.AddReference('ProtoGeometry')
from Autodesk.DesignScript.Geometry import *
OUT = "Hello " + IN[0]
```

![1543162588787](assets\1543162588787.png)

![1543163066021](assets\1543163066021.png)

## Advanced Custom Nodes

### Zero Touch Node

A Zero Touch Node (ZTN), is a custom node written in C#. A ZTN can be obtained by simply [importing a DLL inside of Dynamo](http://dynamoprimer.com/en/10_Packages/10-5_Zero-Touch.html), all the `public static` methods will automatically appear as Dynamo nodes.

By writing your own ZTN you will benefit of the .NET framework, a solid IDE, debugging tools and lots of libraries, C# nodes are also sometimes more performant than Python ones. This type of node needs to be compiled into a DLL every time you want to make a change, this means your code is more safe if you are gong to distribute it but for small tasks Python nodes might still be a better solution.

You can read more about Zero Touch Nodes here: http://teocomi.com/dynamo-unchained-1-learn-how-to-develop-zero-touch-nodes-in-csharp/

### Explicit Custom Node

Explicit nodes are C# nodes that implement the `:NodeModel` interface, they allow for a much higher level of control over the node, its execution and UI and are therefore quite complex to implement. You can read more about them here: http://teocomi.com/dynamo-unchained-1-learn-how-to-develop-zero-touch-nodes-in-csharp/



## Packages

One ore more Custom Nodes, can be organized inside a Dynamo package to be easily distributed. Packages can be installed from the Package Manager, but you can also distribute them in other ways.

Packages can also be browsed from: https://www.dynamopackages.com/

### Creating a package

Let's go ahead and create one!

Creating and publishing packages can only be done from Dynamo for Revit as it's linked to your Autodesk account.

![1543171699741](assets\1543171699741.png)

The add the dyn files of the Custom Nodes we have created earlier and fill in the other fields.

![1543171800719](assets\1543171800719.png)

For now we'll publish the package locally, as **packages published online cannot be removed**.

Select the default packages folder which is:

`C:\Users\YOURUSERNAME\AppData\Roaming\Dynamo\Dynamo Revit\2.0\packages`

In this way it will be picked up automatically by Dynamo.

![1543172382496](assets\1543172382496.png)



### Updating a package

To update an already published package, open the package manager and the click Publish version...



![1543173299941](assets\1543173299941.png)

Then just follow the same procedure seen above.

It's important to use the version numbers in a sensible way, a good convention to do so is semantic versioning, meaning that each of the version numbers corresponds to:

- MAJOR changes when you make incompatible changes
- MINOR changes when you add functionality in a backwards-compatible manner

- PATCH changes when you make backwards-compatible bug fixes.



### Tips & Tricks

#### Distribute your custom nodes together with the graph

Without publishing a node, a Dynamo graph which references a custom node must also have that custom node in its folder (or the custom node must be imported into Dynamo using *File>Import Library*).

#### Manage the packages library paths

The default directory where Dynamo looks for packages is:



But you can edit it or add other other paths from:



![1543173820880](assets\1543173820880.png)

![1543173870254](assets\1543173870254.png)

You could for instance add a path on a shared drive where your company wide packages are stored.

#### Only publish when ready

Packages on the package manager cannot be removed or renamed. Only publish when ready and after having tested you package fully!

#### Explore the packages list as Json

You can manually retrieve the list of dynamo packages from this url, interestingly enough it also contains the url to the download of the package.

**Before proceeding note that's a 3MB json file!** 

http://dynamopackages.com/packages/dynamo

![1543173460457](assets\1543173460457.png)



#### Create a package programmatically

If you are creating a Custom Node in .NET with Visual Studio it's very simple to create a package programmatically.

Dynamo packages have the structure below:

![1543175498922](assets\1543175498922.png)

- The *bin* folder houses .dll files created with C# or Zero-Touch libraries
- The *dyf* folder houses the custom nodes, we won’t have any for this package
- The *extra* folder houses all additional files. These files are likely to be Dynamo Files (.dyn) or any additional files required (.svg, .xls, .jpeg, .sat, etc.)
- The *pkg* file is a basic text file defining the package settings. [This is can be automated by Dynamo](http://dynamoprimer.com/en/10_Packages/10-4_Publishing.html), but we will make one from scratch

We need to manually create the pkg.json file, but we can automate the folder creation using built actions, you can use the below as template :

```json
{
  "license": "",
  "file_hash": null,
  "name": "MySamplePackage",
  "version": "1.0.0",
  "description": "Sample Dynamo package",
  "group": "MySamplePackage",
  "keywords": null,
  "dependencies": [],
  "contents": "",
  "engine_version": "2.0.1.5065",
  "engine": "dynamo",
  "engine_metadata": "",
  "site_url": "",
  "repository_url": "",
  "contains_binaries": false,
  "node_libraries": []
}
```

Then we can configure the visual studio project so that the files are copied automatically into the Dynamo packages folder  after each build in order to Debug our dlls.

Right click on the project > Properties > Debug > Build Events > Post-build event command line > paste the two following lines:

`xcopy /Y "$(TargetDir)*.*" "$(AppData)\Dynamo\Dynamo Revit\2.0\packages\$(ProjectName)\bin\"`

`xcopy /Y "$(ProjectDir)pkg.json" "$(AppData)\Dynamo\Dynamo Revit\2.0\packages\$(ProjectName)"`