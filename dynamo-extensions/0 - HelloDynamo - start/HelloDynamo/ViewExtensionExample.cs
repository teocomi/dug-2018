﻿using Dynamo.ViewModels;
using Dynamo.Wpf.Extensions;
using System;
using System.Windows;
using System.Windows.Controls;

namespace HelloDynamo
{
  /// <summary>
  /// Dynamo View Extension that can control both the Dynamo application and its UI (menus, view, canvas, nodes).
  /// </summary>
  public class ViewExtensionExample : IViewExtension
  {
    public string UniqueId => "5E85F38F-0A19-4F24-9E18-96845764780C";
    public string Name => "Hello Dynamo View Extension";

    private NodeTracker _nodeTracker = null;

    /// <summary>
    /// Method that is called when Dynamo starts, but is not yet ready to be used.
    /// </summary>
    /// <param name="vsp">Parameters that provide references to Dynamo settings, version and extension manager.</param>
    public void Startup(ViewStartupParams vsp) { }

    /// <summary>
    /// Method that is called when Dynamo has finished loading and the UI is ready to be interacted with.
    /// </summary>
    /// <param name="vlp">
    /// Parameters that provide references to Dynamo commands, settings, events and
    /// Dynamo UI items like menus or the background preview. This object is supplied by Dynamo itself.
    /// </param>
    public void Loaded(ViewLoadedParams vlp)
    {

      var viewModel = new NodeTrackerViewModel(vlp);
      _nodeTracker = new NodeTracker
      {
        Owner = vlp.DynamoWindow,
        DataContext = viewModel
      };


      // let's now create a completely top-level new menu item
      var extensionMenu = new MenuItem { Header = "Workshop" };

      // and now we add a new sub-menu item that says hello when clicked
      var sayHelloMenuItem = new MenuItem { Header = "Say Hello" };
      sayHelloMenuItem.Click += (sender, args) =>
      {
        MessageBox.Show("Hello " + Environment.UserName);
      };
      extensionMenu.Items.Add(sayHelloMenuItem);
      //new menu item for our node tracker
      var nodeTrackerMenuItem = new MenuItem { Header = "Node Tracker" };
      nodeTrackerMenuItem.Click += (sender, args) =>
      {
        _nodeTracker.Show();
      };
      extensionMenu.Items.Add(nodeTrackerMenuItem);



      //commands
      var commandsMenuItem = new MenuItem { Header = "Commands Sample" };
      var dynViewModel = vlp.DynamoWindow.DataContext as DynamoViewModel;

      var addNodeMenuItem = new MenuItem { Header = "Add Node" };
      addNodeMenuItem.Click += (sender, args) =>
      {
        var command = new Dynamo.Models.DynamoModel.CreateNodeCommand(Guid.NewGuid().ToString(), "Number Slider", 100, 100, true, false);
        dynViewModel.ExecuteCommand(command);
      };
      var deleteSelectedNodeMenuItem = new MenuItem { Header = "Delete Selected Node" };
      deleteSelectedNodeMenuItem.Click += (sender, args) =>
      {
        dynViewModel.DeleteCommand.Execute(null);
      };
      var zoomSelectedNodeMenuItem = new MenuItem { Header = "Zoom Selected Node" };
      zoomSelectedNodeMenuItem.Click += (sender, args) =>
      {
        dynViewModel.FitViewCommand.Execute(null);
      };

      commandsMenuItem.Items.Add(addNodeMenuItem);
      commandsMenuItem.Items.Add(deleteSelectedNodeMenuItem);
      commandsMenuItem.Items.Add(zoomSelectedNodeMenuItem);

      extensionMenu.Items.Add(commandsMenuItem);


      // finally, we need to add our menu to Dynamo
      vlp.dynamoMenu.Items.Add(extensionMenu);
    }

    /// <summary>
    /// Method that is called when the host Dynamo application is closed.
    /// </summary>
    public void Shutdown() { }

    public void Dispose() { }
  }
}
