---

---

# Create your first Dynamo Extension and customize Dynamo even more!



## Summary

Extensions are a new feature of the Dynamo API, they let you interact with the entire Dynamo environment. From searching the canvas for placed nodes to adding physics simulation, the community has been having a blast extending Dynamo! 

In this lab you’ll learn how to build one in C# in Visual Studio, we'll see how extensions can be used to interact with nodes on the canvas, with the graph itself and with the Dynamo View Model. We'll also see how to set up Visual Studio to automatically build and bundle the extension with other packages for release on the package manager.

### Learning objectives

- Learn what extensions are and how they relate to custom nodes
- Learn how to write your own Dynamo Extension to affect the Dynamo Graph and nodes
- Learn how to publish an Extension with the Package Manager



## Requirements

- Visual Studio 2015 or 2017
- Dynamo 2.0.1



## Anatomy of an Extension

From `HelloDynamo\` open `HelloDynamo.sln` in Visual Studio.

The project HelloDynamo inside the solution has already been configured with all the required files and settings to run a simple extension (thanks Eric!). Let's see them!

### Project Settings

![1541164521433](assets/1541164521433.png)

We are of course building to Class Library, and note that the **.NET Framework has been set to 4.5**, that's because Dynamo itself is using v4.5, and therefore it can't be higher than that.

### References

![1541164719392](assets/1541164719392.png)

References are managed via NuGet, if you were to start a project from scratch you'll just need to add:

- `DynamoVisualProgramming.Core`

- `DynamoVisualProgramming.WpfUILibrary`

as the others will be added automatically as dependencies.

Always remember to set these new references to `Copy Local = False`.

![1541171710594](assets/1541171710594.png)

### Build Events

We'll go thorough build events later in this workshop, but just for you to know, there is some magical code in your `HelloDynamo.csproj` file that automatically copies the built files to the right places, so that you don't have to do it manually each time!

### Start Action

Upon debugging we'll run our extension inside DynamoSandbox. This is a version of Dynamo that runs standalone and doesn't have any Revit dependencies and therefore Revit elements/nodes. It is installed together Dynamo for Revit and you can find it in: `C:\Program Files\Dynamo\Dynamo Core\2\DynamoSandbox.exe`

![1541165003441](assets/1541165003441.png)

### Project Structure

There's not really a project structure to follow, but let's see how HelloDynamo has been set up.

![A8B4FB2F-5609-4D72-95AE-8D54764ED26F](assets/A8B4FB2F-5609-4D72-95AE-8D54764ED26F.png)

This sample project contains both a sample Extension and a sample ViewExtension therefore we can find:

- `ExtensionExample.cs` is the code for a sample Extension
- `ViewExtensionExample.cs` is the code for a sample ViewExtension
- `HelloDynamo_ExtensionDefinition.xml` is a manifest file which tells Dynamo which class to instantiate to start the Extension
- `HelloDynamo_ViewExtensionDefinition.xml` is a manifest which tells Dynamo which class to instantiate to start the ViewExtension

It also contains a:

- `packages.config` used by NuGet
- `pkg.json` used to define a Dynamo package



## Hello Dynamo!

Now let's have a look at the code.

### The IExtension and IViewExtension interfaces

The two classes we saw above are respectively implementing the `IExtension ` and `IViewExtension` interfaces and the methods/properties they specify.

```c#
using System.Windows;
using Dynamo.Extensions;

namespace HelloDynamo
{
  public class ExtensionExample : IExtension
  {
    public string UniqueId => "3B234622-43B7-4EA8-86DA-54FB390BE29E";

    public string Name => "Hello World";

    public void Dispose() { }

    public void Ready(ReadyParams rp)
    {
      MessageBox.Show("Extension is ready!");
    }

    public void Shutdown() { }

    public void Startup(StartupParams sp) { }
  }
}

```

```c#
using System.Windows;
using Dynamo.Wpf.Extensions;

namespace HelloDynamo
{
  public class ViewExtensionExample : IViewExtension
  {
    public string UniqueId => "5E85F38F-0A19-4F24-9E18-96845764780C";

    public string Name => "Hello Dynamo";

    public void Loaded(ViewLoadedParams p)
    {
      MessageBox.Show("ViewExtension has loaded!");
    }

    public void Dispose() { }

    public void Shutdown() { }

    public void Startup(ViewStartupParams p) { }
  }
}

```

At startup Dynamo calls two methods on the extensions:

- `Startup(StartupParams)` or `Startup(ViewStartupParams)` called when Dynamo starts loading
- `Ready(ReadyParams)` or `Loaded(ViewLoadedParams)` called when Dynamo is finished loading

Referring then to this diagram, you should now have an idea of what will happen when we debug (image courtesy of the Dynamo team):



![1541180063477](assets/1541180063477.png)

**Test run #1**

Let's click F5 and Debug to see this in action...

![1541180543610](assets/1541180543610.png)

![1541180564931](assets/1541180564931.png)

As you can se the StartUp event in the extension preceeded the one of the view extension.

#### Params

The parameters passed in the `StartUp`, `Loaded` and `Ready` events are what will give us access to all the powerful things extensions can do. Below some of the main ones:

**StartupParams**

- AuthProvider
- Preferences
- PathManager
- LibraryLoader
- CustomNodeManager
- DynamoVersion

**ViewStartupParams**

- All the above (it inherits from StartupParams)
- ExtensionManager

**ReadyParams**

- WorkspaceModels
- CurrentWorkspaceModel
- CommandExecutive
- NotificationRecieved
- CurrentWorkspaceChanged

**ViewLoadedParams**

- All the above (it inherits from ReadyParams)
- DynamoMenu
- BackGroundPreviewViewModel
- RenderPackageFactory
- DynamoWindow
- CommandExecutive
- AddMenuItem
- SelectionCollectionChanged

### Adding a menu item

Let's see an example, let's add a menu item. This is pretty simple, the `ViewStartupParams.dynamoMenu` field gives us access to to the Dynamo menu, to add a new menu item we just need. :

```c#
      // let's now create a completely top-level new menu item
      extensionMenu = new MenuItem { Header = "Workshop" };

      // and now we add a new sub-menu item that says hello when clicked
      var sayHelloMenuItem = new MenuItem { Header = "Say Hello" };
      sayHelloMenuItem.Click += (sender, args) =>
      {
        MessageBox.Show("Hello " + Environment.UserName);
      };
      extensionMenu.Items.Add(sayHelloMenuItem);

      // finally, we need to add our menu to Dynamo
      viewLoadedParams.dynamoMenu.Items.Add(extensionMenu);
```

**Test run #2**

Debug and see our new menu item:

![1542910603807](assets/1542910603807.png)      

### MVVM

![img](https://cdn-images-1.medium.com/max/1600/1*8KprSpqqPtSuYObjOFPt2g.png)

The Model-View-View-Model pattern is very popular and helps to have a good architecture in our projects, if you’re unfamiliar with it or data binding, here is brief introduction.

The MVVM design pattern is similar to the well known MVC pattern in that the *M* (Model) and *V* (View) are *relatively* the same. The only difference resides between the *C* (Controller) and the *VM* (View Model).

**Model**

Represents the *Data + State + Business logic*. It is not tied to the view nor to the controller, which makes it reusable in many contexts.

**View**

*Binds* to observable variables and actions exposed by the View Model. It is possible for multiple views to bind to a single View Model.

**View Model**

Responsible for wrapping the model and preparing observable data needed by the view. It also provides hooks for the view to pass events to the model. An important thing to keep in mind is that *the View Model is not tied to the view*.

### Tracking changes

Let's now explore another very useful functionality available via the `ViewStartupParams` while making use of the MVVM pattern and data binding. 

With  `ViewStartupParams.CurrentWorkspace` we can access several events:

```c#
viewLoadedParams.CurrentWorkspaceChanged
viewLoadedParams.NotificationRecieved
viewLoadedParams.CurrentWorkspaceModel.NodeAdded
viewLoadedParams.CurrentWorkspaceModel.NodeRemoved
viewLoadedParams.CurrentWorkspaceModel.ConnectorAdded
viewLoadedParams.CurrentWorkspaceModel.ConnectorDeleted
```

Let's edit our ViewExtension to make use of them. Create a `NodeTracker.xaml` window which will contain the list of our tracked events. This will be our **View**.

```xml
<Window x:Class="HelloDynamo.NodeTracker"
        xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
        xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
        xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
        xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
        xmlns:local="clr-namespace:HelloDynamo"
        mc:Ignorable="d"
        Title="NodeTracker" Height="450" Width="400">
  <Grid>
    <ListView ItemsSource="{Binding Actions}"></ListView>
  </Grid>
</Window>
```

Where `NodeTracker.xaml.cs` looks like:

```C#
using System.Windows;

namespace HelloDynamo
{
  /// <summary>
  /// Tracks and displays events for node/connector added/removed 
  /// </summary>
  public partial class NodeTracker : Window
  {
    public NodeTracker()
    {
      InitializeComponent();
      this.Closing += NodeTracker_Closing;
    }

    private void NodeTracker_Closing(object sender, System.ComponentModel.CancelEventArgs e)
    {
      //hide window instead of closing it, so we keep tracking in the background
      e.Cancel = true;
      this.Hide();
    }
  }
}
```

Now let's crate a **ViewModel** that will bind to the window and will keep track of the changes to the `CurrentWorkspaceModel`, create a new class `NodeTrackerViewModel.cs`:

```C#
using System;
using System.Collections.ObjectModel;
using Dynamo.Core;
using Dynamo.Extensions;

namespace HelloDynamo
{
  public class NodeTrackerViewModel: NotificationObject, IDisposable

  {
    private ReadyParams readyParams;

    private ObservableCollection<string> _actions = new ObservableCollection<string> ();
    public ObservableCollection<string> Actions { get { return _actions; } set { _actions = value; RaisePropertyChanged("Actions"); } }

    public NodeTrackerViewModel(ReadyParams p)
    {
      readyParams = p;
      
      //subscribing to dynamo events
      readyParams.CurrentWorkspaceModel.NodeAdded += CurrentWorkspaceModel_NodeAdded;
      readyParams.CurrentWorkspaceModel.NodeRemoved += CurrentWorkspaceModel_NodeRemoved;
      readyParams.CurrentWorkspaceModel.ConnectorAdded += CurrentWorkspaceModel_ConnectorAdded;
      readyParams.CurrentWorkspaceModel.ConnectorDeleted += CurrentWorkspaceModel_ConnectorDeleted;

      //making sure the binding is updated when elements are added and removed
      Actions.CollectionChanged += Actions_CollectionChanged;
    }

    private void Actions_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
    {
      RaisePropertyChanged("Actions");
    }

    private void CurrentWorkspaceModel_ConnectorDeleted(Dynamo.Graph.Connectors.ConnectorModel obj)
    {
      Actions.Add($"Connector between port {obj.Start.Name} and {obj.End.Name} deleted");
    }

    private void CurrentWorkspaceModel_ConnectorAdded(Dynamo.Graph.Connectors.ConnectorModel obj)
    {
      Actions.Add($"Connector between port {obj.Start.Name} and {obj.End.Name} added");
    }

    private void CurrentWorkspaceModel_NodeRemoved(Dynamo.Graph.Nodes.NodeModel obj)
    {
      Actions.Add($"Node {obj.Name} deleted");
    }

    private void CurrentWorkspaceModel_NodeAdded(Dynamo.Graph.Nodes.NodeModel obj)
    {
      Actions.Add($"Node {obj.Name} created");
    }

    public void Dispose()
    {
      //unsubscribing from events
      readyParams.CurrentWorkspaceModel.NodeAdded -= CurrentWorkspaceModel_NodeAdded;
      readyParams.CurrentWorkspaceModel.NodeRemoved -= CurrentWorkspaceModel_NodeRemoved;
      readyParams.CurrentWorkspaceModel.ConnectorAdded -= CurrentWorkspaceModel_ConnectorAdded;
      readyParams.CurrentWorkspaceModel.ConnectorDeleted -= CurrentWorkspaceModel_ConnectorDeleted;
    }

  }
}
```

Each time a node or connector is added or removed we log that event by adding a string to our `Actions` collection, in this case an ObservableCollection so that we can bind it to the `ListView` in the` NodeTracker` window.

Finally let's edit `ViewExtensionExample.cs` to start the tracking and to open the window when the user clicks on a menu item:

```C#
using Dynamo.Wpf.Extensions;
using System;
using System.Windows;
using System.Windows.Controls;

namespace HelloDynamo
{
  /// <summary>
  /// Dynamo View Extension that can control both the Dynamo application and its UI (menus, view, canvas, nodes).
  /// </summary>
  public class ViewExtensionExample : IViewExtension
  {
    public string UniqueId => "5E85F38F-0A19-4F24-9E18-96845764780C";
    public string Name => "Hello Dynamo View Extension";

    private MenuItem extensionMenu;
    private ViewLoadedParams viewLoadedParams;
    
    private NodeTracker _nodeTracker = null;

    public void Startup(ViewStartupParams vsp) { }

    public void Loaded(ViewLoadedParams vlp)
    {
      viewLoadedParams = vlp;
	  //instanciating the window and setting the datacontext to bind it to the viewmodel
      var viewModel = new NodeTrackerViewModel(viewLoadedParams);
      _nodeTracker = new NodeTracker
      {
        Owner = viewLoadedParams.DynamoWindow,
        DataContext = viewModel
      };

      MakeMenuItems();
    }

    public void MakeMenuItems()
    {
      extensionMenu = new MenuItem { Header = "Workshop" };

      var sayHelloMenuItem = new MenuItem { Header = "Say Hello" };
      sayHelloMenuItem.Click += (sender, args) =>
      {
         MessageBox.Show("Hello " + Environment.UserName);
      };
      //new menu item for our node tracker
      var nodeTrackerMenuItem = new MenuItem { Header = "Node Tracker" };
      nodeTrackerMenuItem.Click += (sender, args) =>
      {
        _nodeTracker.Show();
      };

      extensionMenu.Items.Add(sayHelloMenuItem);
      extensionMenu.Items.Add(nodeTrackerMenuItem);
      viewLoadedParams.dynamoMenu.Items.Add(extensionMenu);
    }

    public void Shutdown() { }

    public void Dispose() { }
  }
}
```

**Test run #3**

![82A0CB3E-3AD3-4DDC-A423-32C089B471B8](assets/82A0CB3E-3AD3-4DDC-A423-32C089B471B8.png)



### Commands

To interact even further with the graph and the nodes the extensions API exposes some useful commands.

For instance, to delete the current selection you just need to call:

`dynViewModel.DeleteCommand.Execute(null);`

To fit the view:

`dynViewModel.FitViewCommand.Execute(null);`

And to create a new node:

```c#
var command = new Dynamo.Models.DynamoModel.CreateNodeCommand(Guid.NewGuid().ToString(), "Number Slider", 100, 100, true, false);
dynViewModel.ExecuteCommand(command);
```

Let's see that in action! Add the following code to the bottom of `MakeMenuItems()`:

```c#
//commands
var commandsMenuItem = new MenuItem { Header = "Commands Sample" };
var dynViewModel = viewLoadedParams.DynamoWindow.DataContext as DynamoViewModel;

var addNodeMenuItem = new MenuItem { Header = "Add Node" };
addNodeMenuItem.Click += (sender, args) =>
{
  var command = new Dynamo.Models.DynamoModel.CreateNodeCommand(Guid.NewGuid().ToString(), "Number Slider", 100, 100, true, false);
  dynViewModel.ExecuteCommand(command);
};
var deleteSelectedNodeMenuItem = new MenuItem { Header = "Delete Selected Node" };
deleteSelectedNodeMenuItem.Click += (sender, args) =>
{
  dynViewModel.DeleteCommand.Execute(null);
};
var zoomSelectedNodeMenuItem = new MenuItem { Header = "Zoom Selected Node" };
zoomSelectedNodeMenuItem.Click += (sender, args) =>
{
  dynViewModel.FitViewCommand.Execute(null);
};

commandsMenuItem.Items.Add(addNodeMenuItem);
commandsMenuItem.Items.Add(deleteSelectedNodeMenuItem);
commandsMenuItem.Items.Add(zoomSelectedNodeMenuItem);

extensionMenu.Items.Add(sayHelloMenuItem);
extensionMenu.Items.Add(nodeTrackerMenuItem);
extensionMenu.Items.Add(commandsMenuItem);
viewLoadedParams.dynamoMenu.Items.Add(extensionMenu);
```



**Test run #4**

![commands](assets/commands.gif)

 